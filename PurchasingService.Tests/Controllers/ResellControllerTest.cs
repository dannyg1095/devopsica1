﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PurchasingService.Controllers;
using PurchasingService.DTOs;
using PurchasingService.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace PurchasingService.Tests.Controllers
{
    [TestClass]
    public class ResellControllerTest
    {
        [TestMethod]
        public void GetResellTest1()
        {
            //Arrange
            var ResellRepo = new Mock<IResellRepository>();

            List<Resell> Resells = new List<Resell>();
            Resells.Add(new Resell(1, "Ean", 1.00, DateTime.Now));
            Resells.Add(new Resell(2, "Ean", 2.00, DateTime.Now.AddDays(-1)));
            Resells.Add(new Resell(3, "Ean", 3.00, DateTime.Now.AddDays(-2)));
            Resells.Add(new Resell(4, "Ean", 4.00, DateTime.Now.AddDays(-3)));
            Resells.Add(new Resell(5, "Ean", 5.00, DateTime.Now.AddDays(-4)));
            Resells.Add(new Resell(6, "Ean", 6.00, DateTime.Now.AddDays(-5)));
            Resells.Add(new Resell(7, "Ean", 7.00, DateTime.Now.AddDays(-6)));
            Resells.Add(new Resell(8, "Ean", 8.00, DateTime.Now.AddDays(-7)));
            Resells.Add(new Resell(9, "Ean", 9.00, DateTime.Now.AddDays(-8)));


            ResellRepo.Setup(rr => rr.GetByDate(It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(Resells);

            var rc = new ResellController(ResellRepo.Object);

            //Act
            List<Resell> Actual = rc.GetResell();

            //Assert
            List<Resell> Expected = new List<Resell>();
            Expected.AddRange(Resells);

            CollectionAssert.AreEqual(Expected, Actual);
        }

        [TestMethod]
        public void GetResellTest2()
        {
            //Arrange
            var ResellRepo = new Mock<IResellRepository>();

            List<Resell> Resells = new List<Resell>();
            Resells.Add(new Resell(1, "Ean", 1.00, DateTime.Now));
            Resells.Add(new Resell(2, "Ean", 2.00, DateTime.Now.AddDays(-1)));
            Resells.Add(new Resell(3, "Ean", 3.00, DateTime.Now.AddDays(-2)));
            Resells.Add(new Resell(4, "Ean", 4.00, DateTime.Now.AddDays(-3)));
            Resells.Add(new Resell(5, "Ean", 5.00, DateTime.Now.AddDays(-4)));
            Resells.Add(new Resell(6, "Ean", 6.00, DateTime.Now.AddDays(-5)));
            Resells.Add(new Resell(7, "Ean", 7.00, DateTime.Now.AddDays(-6)));
            Resells.Add(new Resell(8, "Ean", 8.00, DateTime.Now.AddDays(-7)));
            Resells.Add(new Resell(9, "Ean", 9.00, DateTime.Now.AddDays(-8)));


            ResellRepo.Setup(rr => rr.GetByDate(It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(Resells.FindAll(r => r.Date >= DateTime.Now.AddDays(-3)));

            var rc = new ResellController(ResellRepo.Object);

            //Act
            List<Resell> Actual = rc.GetResell(DateTime.Now.AddDays(-3), null);

            //Assert
            List<Resell> Expected = new List<Resell>();
            Expected.AddRange(Resells.FindAll(r => r.Date >= DateTime.Now.AddDays(-3)));

            CollectionAssert.AreEqual(Expected, Actual);
        }

        [TestMethod]
        public void GetResellTest3()
        {
            //Arrange
            var ResellRepo = new Mock<IResellRepository>();

            List<Resell> Resells = new List<Resell>();
            Resells.Add(new Resell(1, "Ean", 1.00, DateTime.Now));
            Resells.Add(new Resell(2, "Ean", 2.00, DateTime.Now.AddDays(-1)));
            Resells.Add(new Resell(3, "Ean", 3.00, DateTime.Now.AddDays(-2)));
            Resells.Add(new Resell(4, "Ean", 4.00, DateTime.Now.AddDays(-3)));
            Resells.Add(new Resell(5, "Ean", 5.00, DateTime.Now.AddDays(-4)));
            Resells.Add(new Resell(6, "Ean", 6.00, DateTime.Now.AddDays(-5)));
            Resells.Add(new Resell(7, "Ean", 7.00, DateTime.Now.AddDays(-6)));
            Resells.Add(new Resell(8, "Ean", 8.00, DateTime.Now.AddDays(-7)));
            Resells.Add(new Resell(9, "Ean", 9.00, DateTime.Now.AddDays(-8)));


            ResellRepo.Setup(rr => rr.GetByDate(It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(Resells.FindAll(r => r.Date <= DateTime.Now.AddDays(-7)));

            var rc = new ResellController(ResellRepo.Object);

            //Act
            List<Resell> Actual = rc.GetResell(null, DateTime.Now.AddDays(-7));

            //Assert
            List<Resell> Expected = new List<Resell>();
            Expected.AddRange(Resells.FindAll(r => r.Date <= DateTime.Now.AddDays(-7)));

            CollectionAssert.AreEqual(Expected, Actual);
        }

        [TestMethod]
        public void GetResellTest4()
        {
            //Arrange
            var ResellRepo = new Mock<IResellRepository>();

            List<Resell> Resells = new List<Resell>();
            Resells.Add(new Resell(1, "Ean", 1.00, DateTime.Now));
            Resells.Add(new Resell(2, "Ean", 2.00, DateTime.Now.AddDays(-1)));
            Resells.Add(new Resell(3, "Ean", 3.00, DateTime.Now.AddDays(-2)));
            Resells.Add(new Resell(4, "Ean", 4.00, DateTime.Now.AddDays(-3)));
            Resells.Add(new Resell(5, "Ean", 5.00, DateTime.Now.AddDays(-4)));
            Resells.Add(new Resell(6, "Ean", 6.00, DateTime.Now.AddDays(-5)));
            Resells.Add(new Resell(7, "Ean", 7.00, DateTime.Now.AddDays(-6)));
            Resells.Add(new Resell(8, "Ean", 8.00, DateTime.Now.AddDays(-7)));
            Resells.Add(new Resell(9, "Ean", 9.00, DateTime.Now.AddDays(-8)));


            ResellRepo.Setup(rr => rr.GetByDate(It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(Resells.FindAll(r => r.Date <= DateTime.Now.AddDays(-1) && r.Date >= DateTime.Now.AddDays(-3)));

            var rc = new ResellController(ResellRepo.Object);

            //Act
            List<Resell> Actual = rc.GetResell(DateTime.Now.AddDays(-1), DateTime.Now.AddDays(-3));

            //Assert
            List<Resell> Expected = new List<Resell>();
            Expected.AddRange(Resells.FindAll(r => r.Date <= DateTime.Now.AddDays(-1) && r.Date >= DateTime.Now.AddDays(-3)));

            CollectionAssert.AreEqual(Expected, Actual);
        }

        [TestMethod]
        public void GetResellByEanTest()
        {
            //Arrange
            var ResellRepo = new Mock<IResellRepository>();

            List<Resell> Resells = new List<Resell>();
            Resells.Add(new Resell(1, "654254654", 1.00, DateTime.Now));
            Resells.Add(new Resell(2, "654254654", 2.00, DateTime.Now.AddDays(-1)));
            Resells.Add(new Resell(3, "654254654", 3.00, DateTime.Now.AddDays(-2)));
            Resells.Add(new Resell(4, "807324653", 4.00, DateTime.Now.AddDays(-3)));
            Resells.Add(new Resell(5, "807324653", 5.00, DateTime.Now.AddDays(-4)));
            Resells.Add(new Resell(6, "807324653", 6.00, DateTime.Now.AddDays(-5)));
            Resells.Add(new Resell(7, "763567456", 7.00, DateTime.Now.AddDays(-6)));
            Resells.Add(new Resell(8, "763567456", 8.00, DateTime.Now.AddDays(-7)));
            Resells.Add(new Resell(9, "763567456", 9.00, DateTime.Now.AddDays(-8)));

            String Ean = "654254654";

            ResellRepo.Setup(rr => rr.GetByEan(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(Resells.FindAll(r => r.EAN.Equals(Ean)));

            var rc = new ResellController(ResellRepo.Object);

            //Act
            List<Resell> Actual = rc.GetResellByEan(Ean);

            //Assert
            List<Resell> Expected = Resells.FindAll(r => r.EAN.Equals(Ean));

            CollectionAssert.AreEqual(Expected, Actual);
        }

        [TestMethod]
        public void GetResellByIdTest()
        {
            //Arrange
            var ResellRepo = new Mock<IResellRepository>();

            List<Resell> Resells = new List<Resell>();
            Resells.Add(new Resell(1, "654254654", 1.00, DateTime.Now));
            Resells.Add(new Resell(1, "654254654", 2.00, DateTime.Now.AddDays(-1)));
            Resells.Add(new Resell(1, "654254654", 3.00, DateTime.Now.AddDays(-2)));
            Resells.Add(new Resell(1, "807324653", 4.00, DateTime.Now.AddDays(-3)));
            Resells.Add(new Resell(3, "807324653", 5.00, DateTime.Now.AddDays(-4)));
            Resells.Add(new Resell(3, "807324653", 6.00, DateTime.Now.AddDays(-5)));
            Resells.Add(new Resell(3, "763567456", 7.00, DateTime.Now.AddDays(-6)));
            Resells.Add(new Resell(2, "763567456", 8.00, DateTime.Now.AddDays(-7)));
            Resells.Add(new Resell(2, "763567456", 9.00, DateTime.Now.AddDays(-8)));

            int EmployeeId = 3;

            ResellRepo.Setup(rr => rr.GetById(It.IsAny<int>(), It.IsAny<DateTime>(), It.IsAny<DateTime>())).Returns(Resells.FindAll(r => r.EmployeeId == EmployeeId));

            var rc = new ResellController(ResellRepo.Object);

            //Act
            List<Resell> Actual = rc.GetResellById(EmployeeId);

            //Assert
            List<Resell> Expected = Resells.FindAll(r => r.EmployeeId == EmployeeId);

            CollectionAssert.AreEqual(Expected, Actual);
        }
    }
}
