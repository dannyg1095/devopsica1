﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PurchasingService.Controllers;
using PurchasingService.DTOs;
using PurchasingService.Interfaces;
using PurchasingService.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService.Tests.Controllers
{
    [TestClass]
    public class PurchasingControllerTest
    {
        [TestMethod]
        public void GetProductsTest1()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            List<Product> Products1 = new List<Product>();
            List<Product> Products2 = new List<Product>();
            Products1.Add(new Product(1, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 1.00, true, null));
            Products1.Add(new Product(2, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 2.00, true, null));
            Products1.Add(new Product(3, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 3.00, true, null));
            Products1.Add(new Product(4, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 4.00, true, null));
            Products1.Add(new Product(5, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 5.00, true, null));
            Products2.Add(new Product(6, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 6.00, true, null));
            Products2.Add(new Product(7, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 7.00, true, null));
            Products2.Add(new Product(8, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 8.00, true, null));
            Products2.Add(new Product(9, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 9.00, true, null));
            Products2.Add(new Product(10, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 10.00, true, null));

            undercutters.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products1));
            dodgydealers.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products2));

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            List<Product> Actual = pc.GetProducts(null, null, null, null);

            //Assert
            List<Product> Expected = new List<Product>();
            Expected.AddRange(Products1);
            Expected.AddRange(Products2);

            Assert.AreEqual(Expected.ToString(), Actual.ToString()); //ToString so that they're comporable
        }

        [TestMethod]
        public void GetProductsTest2()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            List<Product> Products1 = new List<Product>();
            List<Product> Products2 = new List<Product>();
            Products1.Add(new Product(1, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 1.00, true, null));
            Products1.Add(new Product(2, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 2.00, true, null));
            Products1.Add(new Product(3, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 3.00, true, null));
            Products1.Add(new Product(4, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 4.00, true, null));
            Products1.Add(new Product(5, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 5.00, true, null));
            Products2.Add(new Product(6, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 6.00, true, null));
            Products2.Add(new Product(7, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 7.00, true, null));
            Products2.Add(new Product(8, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 8.00, true, null));
            Products2.Add(new Product(9, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 9.00, true, null));
            Products2.Add(new Product(10, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 10.00, true, null));

            undercutters.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products1));
            dodgydealers.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products2));

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            List<Product> Actual = pc.GetProducts(1, null, null, null);

            //Assert
            List<Product> Expected = new List<Product>();
            Expected.AddRange(Products1.FindAll(p => p.CategoryId == 1));
            Expected.AddRange(Products2.FindAll(p => p.CategoryId == 1));

            Assert.AreEqual(Expected.ToString(), Actual.ToString()); //ToString so that they're comporable
        }

        [TestMethod]
        public void GetProductsTest3()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            List<Product> Products1 = new List<Product>();
            List<Product> Products2 = new List<Product>();
            Products1.Add(new Product(1, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 1.00, true, null));
            Products1.Add(new Product(2, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 2.00, true, null));
            Products1.Add(new Product(3, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 3.00, true, null));
            Products1.Add(new Product(4, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 4.00, true, null));
            Products1.Add(new Product(5, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 5.00, true, null));
            Products2.Add(new Product(6, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 6.00, true, null));
            Products2.Add(new Product(7, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 7.00, true, null));
            Products2.Add(new Product(8, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 8.00, true, null));
            Products2.Add(new Product(9, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 9.00, true, null));
            Products2.Add(new Product(10, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 10.00, true, null));

            undercutters.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products1));
            dodgydealers.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products2));

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            List<Product> Actual = pc.GetProducts(null, 1, null, null);

            //Assert
            List<Product> Expected = new List<Product>();
            Expected.AddRange(Products1.FindAll(p => p.BrandId == 1));
            Expected.AddRange(Products2.FindAll(p => p.BrandId == 1));

            Assert.AreEqual(Expected.ToString(), Actual.ToString()); //ToString so that they're comporable
        }

        [TestMethod]
        public void GetProductsTest4()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            List<Product> Products1 = new List<Product>();
            List<Product> Products2 = new List<Product>();
            Products1.Add(new Product(1, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 1.00, true, null));
            Products1.Add(new Product(2, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 2.00, true, null));
            Products1.Add(new Product(3, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 3.00, true, null));
            Products1.Add(new Product(4, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 4.00, true, null));
            Products1.Add(new Product(5, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 5.00, true, null));
            Products2.Add(new Product(6, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 6.00, true, null));
            Products2.Add(new Product(7, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 7.00, true, null));
            Products2.Add(new Product(8, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 8.00, true, null));
            Products2.Add(new Product(9, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 9.00, true, null));
            Products2.Add(new Product(10, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 10.00, true, null));

            undercutters.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products1));
            dodgydealers.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products2));

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            List<Product> Actual = pc.GetProducts(null, null, 1.00, null);

            //Assert
            List<Product> Expected = new List<Product>();
            Expected.AddRange(Products1.FindAll(p => p.Price >= 1.00));
            Expected.AddRange(Products2.FindAll(p => p.Price >= 1.00));

            Assert.AreEqual(Expected.ToString(), Actual.ToString()); //ToString so that they're comporable
        }

        [TestMethod]
        public void GetProductsTest5()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            List<Product> Products1 = new List<Product>();
            List<Product> Products2 = new List<Product>();
            Products1.Add(new Product(1, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 1.00, true, null));
            Products1.Add(new Product(2, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 2.00, true, null));
            Products1.Add(new Product(3, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 3.00, true, null));
            Products1.Add(new Product(4, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 4.00, true, null));
            Products1.Add(new Product(5, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 5.00, true, null));
            Products2.Add(new Product(6, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 6.00, true, null));
            Products2.Add(new Product(7, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 7.00, true, null));
            Products2.Add(new Product(8, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 8.00, true, null));
            Products2.Add(new Product(9, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 9.00, true, null));
            Products2.Add(new Product(10, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 10.00, true, null));

            undercutters.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products1));
            dodgydealers.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products2));

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            List<Product> Actual = pc.GetProducts(null, null, null, 10.00);

            //Assert
            List<Product> Expected = new List<Product>();
            Expected.AddRange(Products1.FindAll(p => p.Price <= 10.00));
            Expected.AddRange(Products2.FindAll(p => p.Price <= 10.00));

            Assert.AreEqual(Expected.ToString(), Actual.ToString()); //ToString so that they're comporable
        }

        [TestMethod]
        public void GetProductsTest6()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            List<Product> Products1 = new List<Product>();
            List<Product> Products2 = new List<Product>();
            Products1.Add(new Product(1, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 1.00, true, null));
            Products1.Add(new Product(2, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 2.00, true, null));
            Products1.Add(new Product(3, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 3.00, true, null));
            Products1.Add(new Product(4, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 4.00, true, null));
            Products1.Add(new Product(5, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 5.00, true, null));
            Products2.Add(new Product(6, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 6.00, true, null));
            Products2.Add(new Product(7, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 7.00, true, null));
            Products2.Add(new Product(8, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 8.00, true, null));
            Products2.Add(new Product(9, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 9.00, true, null));
            Products2.Add(new Product(10, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 10.00, true, null));

            undercutters.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products1));
            dodgydealers.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products2));

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            List<Product> Actual = pc.GetProducts(null, null, 5.00, 10.00);

            //Assert
            List<Product> Expected = new List<Product>();
            Expected.AddRange(Products1.FindAll(p => p.Price <= 10.00 && p.Price >= 5.00));
            Expected.AddRange(Products2.FindAll(p => p.Price <= 10.00 && p.Price >= 5.00));

            Assert.AreEqual(Expected.ToString(), Actual.ToString()); //ToString so that they're comporable
        }

        [TestMethod]
        public void GetProductsTest7()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            List<Product> Products1 = new List<Product>();
            List<Product> Products2 = new List<Product>();
            Products1.Add(new Product(1, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 1.00, true, null));
            Products1.Add(new Product(2, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 2.00, true, null));
            Products1.Add(new Product(3, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 3.00, true, null));
            Products1.Add(new Product(4, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 4.00, true, null));
            Products1.Add(new Product(5, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 5.00, true, null));
            Products2.Add(new Product(6, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 6.00, true, null));
            Products2.Add(new Product(7, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 7.00, true, null));
            Products2.Add(new Product(8, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 8.00, true, null));
            Products2.Add(new Product(9, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 9.00, true, null));
            Products2.Add(new Product(10, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 10.00, true, null));

            undercutters.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products1));
            dodgydealers.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products2));

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            List<Product> Actual = pc.GetProducts(1, 5, null, null);

            //Assert
            List<Product> Expected = new List<Product>();
            Expected.AddRange(Products1.FindAll(p => p.CategoryId == 1 && p.BrandId == 5));
            Expected.AddRange(Products2.FindAll(p => p.CategoryId == 1 && p.BrandId == 5));

            Assert.AreEqual(Expected.ToString(), Actual.ToString()); //ToString so that they're comporable
        }

        [TestMethod]
        public void GetProductsTest8()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            List<Product> Products1 = new List<Product>();
            List<Product> Products2 = new List<Product>();
            Products1.Add(new Product(1, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 1.00, true, null));
            Products1.Add(new Product(2, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 2.00, true, null));
            Products1.Add(new Product(3, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 3.00, true, null));
            Products1.Add(new Product(4, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 4.00, true, null));
            Products1.Add(new Product(5, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 5.00, true, null));
            Products2.Add(new Product(6, "EanString", 1, "SupplierName", 1, "CatName1", 5, "BrandName1", "ProductName", "ShortDesc", 1, 6.00, true, null));
            Products2.Add(new Product(7, "EanString", 1, "SupplierName", 2, "CatName1", 4, "BrandName1", "ProductName", "ShortDesc", 1, 7.00, true, null));
            Products2.Add(new Product(8, "EanString", 1, "SupplierName", 3, "CatName1", 3, "BrandName1", "ProductName", "ShortDesc", 1, 8.00, true, null));
            Products2.Add(new Product(9, "EanString", 1, "SupplierName", 4, "CatName1", 2, "BrandName1", "ProductName", "ShortDesc", 1, 9.00, true, null));
            Products2.Add(new Product(10, "EanString", 1, "SupplierName", 5, "CatName1", 1, "BrandName1", "ProductName", "ShortDesc", 1, 10.00, true, null));

            undercutters.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products1.FindAll(p => p.CategoryId == 1 && p.BrandId == 1 && p.Price >= 5.00 && p.Price <= 10.00)));
            dodgydealers.Setup(uc => uc.GetProductAsync(It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<double?>(), It.IsAny<double?>())).Returns(Task.FromResult(Products2.FindAll(p => p.CategoryId == 1 && p.BrandId == 1 && p.Price >= 5.00 && p.Price <= 10.00)));

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            List<Product> Actual = pc.GetProducts(1, 1, 5.00, 10.00);

            //Assert
            List<Product> Expected = new List<Product>();
            Expected.AddRange(Products1.FindAll(p => p.CategoryId == 1 && p.BrandId == 1 && p.Price >= 5.00 && p.Price <= 10.00));
            Expected.AddRange(Products2.FindAll(p => p.CategoryId == 1 && p.BrandId == 1 && p.Price >= 5.00 && p.Price <= 10.00));

            CollectionAssert.AreEqual(Expected, Actual); //ToString so that they're comporable
        }

        [TestMethod]
        public void GetCategoriesTest()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            List<Category> Categories1 = new List<Category>();
            Categories1.Add(new Category(1, "CatName1", "Desc", 1));
            Categories1.Add(new Category(2, "CatName2", "Desc", 1));
            Categories1.Add(new Category(3, "CatName3", "Desc", 1));
            Categories1.Add(new Category(4, "CatName4", "Desc", 1));
            Categories1.Add(new Category(5, "CatName5", "Desc", 1));
            Categories1.Add(new Category(6, "CatName6", "Desc", 1));

            undercutters.Setup(uc => uc.GetCategoriesAsync()).Returns(Task.FromResult(Categories1));

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            List<Category> Actual = pc.GetCategories();

            //Assert
            List<Category> Expected = new List<Category>();
            Expected.AddRange(Categories1);

            CollectionAssert.AreEqual(Expected, Actual); //ToString so that they're comporable
        }

        public void GetCategoryTest()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            Category Category = new Category(1, "CatName1", "Desc", 1);

            undercutters.Setup(uc => uc.GetCategory(It.IsAny<int>())).Returns(Category);

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            Category Actual = pc.GetCategory(1);

            //Assert
            Category Expected = new Category(Category.Id, Category.Name, Category.Desc, Category.ProdCount);

            Assert.AreEqual(Expected.ToString(), Actual.ToString()); //ToString so that they're comporable
        }

        [TestMethod]
        public void GetBrandsTest()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            List<Brand> Brands1 = new List<Brand>();
            Brands1.Add(new Brand(1, "BrandName1", 1));
            Brands1.Add(new Brand(2, "BrandName2", 1));
            Brands1.Add(new Brand(3, "BrandName3", 1));
            Brands1.Add(new Brand(4, "BrandName4", 1));
            Brands1.Add(new Brand(5, "BrandName5", 1));
            Brands1.Add(new Brand(6, "BrandName6", 1));

            undercutters.Setup(uc => uc.GetBrandsAsync()).Returns(Task.FromResult(Brands1));

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            List<Brand> Actual = pc.GetBrands();

            //Assert
            List<Brand> Expected = new List<Brand>();
            Expected.AddRange(Brands1);

            CollectionAssert.AreEqual(Expected, Actual); //ToString so that they're comporable
        }

        public void GetBrandTest()
        {
            //Arrange
            var undercutters = new Mock<IUndercutters>();
            var dodgydealers = new Mock<IDodgyDealers>();
            var bazzasbazaar = new Mock<IBazzasBazaar>();
            var prp = new Mock<IPurchaseRequestRepository>();

            Brand Brand = new Brand(1, "BrandName1", 1);

            undercutters.Setup(uc => uc.GetBrand(It.IsAny<int>())).Returns(Brand);

            var pc = new PurchasingController(prp.Object, undercutters.Object, dodgydealers.Object, bazzasbazaar.Object);

            //Act
            Category Actual = pc.GetCategory(1);

            //Assert
            Brand Expected = new Brand(Brand.Id, Brand.Name, Brand.ProdCount);

            Assert.AreEqual(Expected.ToString(), Actual.ToString()); //ToString so that they're comporable
        }
    }
}
