﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EmailService;
using InvoiceEmailService.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace InvoiceEmailService.Controllers
{
    [Route("api/email")]
    [ApiController]
    [Authorize]
    public class EmailController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IEmailService _emailService;

        public EmailController(IConfiguration configuration, IEmailService emailService)
        {
            _config = configuration;
            _emailService = emailService;
        }

        [HttpPost]
        public async Task<HttpStatusCode> SendInvoice(Invoice invoice)
        {
            var response = await _emailService.SendEmail(
                invoice.email,
                invoice.name,
                invoice.EmailSubject(),
                invoice.EmailBody(),
                _config.GetSection("APIKey").GetSection("SendGrid").Value
            );

            return response;
        }
    }
}