﻿namespace InvoiceEmailService.DTOs
{
    public class Invoice
    {
        public Invoice(int InvoiceId, string Name, string Email, string Address, double Total, Product[] Products)
        {
            this.id = InvoiceId;
            this.name = Name;
            this.email = Email;
            this.address = Address;
            this.total = Total;
            this.products = Products;
        }

        public int id;
        public string name;
        public string email;
        public string address;
        public double total;
        public Product[] products;


        public string EmailSubject()
        {
            return "Three Amigos Invoice";
        }
        public string EmailBody()
        {
            string bd = "<p>Dear " + name + "<br>";
            bd += "<br>Invoice #: " + id;
            bd += "<br>Bill to: " + name;
            bd += "<br>Ship to: " + name + ", " + address;
            bd += "<br><br><table border='1'>";
            bd += "<tr><th>QUANTITY</th><th>PRODUCT</th><th>UNIT PRICE</th><th>UNIT TOTAL</th></tr>";
            for (int i = 0; i < products.Length; i++)
                bd += "<tr><td>" + products[i].quantity + "</td><td>" + products[i].name + "</td><td>£" + products[i].price + "</td><td>£" + (products[i].price * products[i].quantity) + "</td></tr>";
            bd += "</table><br>Total: £" + total + "</p>";

            return bd;
        }


    }
}
