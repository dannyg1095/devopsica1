﻿namespace InvoiceEmailService.DTOs
{
    public class Product
    {
        public Product(string Name, int Quantity, double Price)
        {
            this.name = Name;
            this.quantity = Quantity;
            this.price = Price;
        }

        public string name;
        public int quantity;
        public double price;
    }
}