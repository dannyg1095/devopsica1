﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InvoiceEmailService.DTOs
{
    public class Log
    {
        public Log(int employeeId, DateTime dateTime, HttpStatusCode httpCode)
        {
            this.employeeId = employeeId;
            this.dateTime = dateTime;
            this.httpCode = httpCode;
        }

        int employeeId;
        DateTime dateTime;
        HttpStatusCode httpCode;
    }
}
