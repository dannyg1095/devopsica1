﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using InvoiceEmailService.Controllers;
using InvoiceEmailService.DTOs;
using System;
using System.Net;
using SendGrid.Helpers.Mail;
using SendGrid;
using System.Threading.Tasks;
using Moq;
using Microsoft.Extensions.Configuration;

namespace EmailService.Tests
{
    [TestClass]
    public class EmailControllerTest
    {
        [TestMethod]
        public void sendInvoiceTest1()
        {
            var mockEmailService = new Mock<IEmailService>();
            var mockConfig = new Mock<IConfiguration>();

            //setup mocks
            mockEmailService
                .Setup(EmailService => EmailService.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(HttpStatusCode.Accepted);
            mockConfig.Setup(s => s.GetSection("APIKey").GetSection("SendGrid").Value).Returns("some api key"); //only here so the object isn't null and stop the test from failing

            //pass mocks to controller
            var emailController = new EmailController(mockConfig.Object, mockEmailService.Object);

            //get result
            var Product1 = new Product("Test Product 1", 3, 2.00);
            var Product2 = new Product("Test Product 2", 1, 5.00);
            var Product3 = new Product("Test Product 3", 10, 1.75);
            var Product4 = new Product("Test Product 4", 5, 11.99);
            var products = new Product[] { Product1, Product2, Product3, Product3, Product4 };
            Invoice invoice = new Invoice(1, "test_name", "q5113823@live.tees.ac.uk", "123 fake street", 1234.43, products); //Note: 'Total price' is not accurate on purpose

            var emailControllerResult = emailController.SendInvoice(invoice).Result;

            Assert.AreEqual(HttpStatusCode.Accepted, emailControllerResult);
        }

        [TestMethod]
        public void sendInvoiceTest2()
        {
            var mockEmailService = new Mock<IEmailService>();
            var mockConfig = new Mock<IConfiguration>();

            //setup mocks
            mockEmailService
                .Setup(EmailService => EmailService.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(HttpStatusCode.Unauthorized);
            mockConfig.Setup(s => s.GetSection("APIKey").GetSection("SendGrid").Value).Returns("INCORRECT API KEY"); //only here so the object isn't null and stop the test from failing

            //pass mocks to controller
            var emailController = new EmailController(mockConfig.Object, mockEmailService.Object);

            //get result
            var Product1 = new Product("Test Product 1", 3, 2.00);
            var Product2 = new Product("Test Product 2", 1, 5.00);
            var Product3 = new Product("Test Product 3", 10, 1.75);
            var Product4 = new Product("Test Product 4", 5, 11.99);
            var products = new Product[] { Product1, Product2, Product3, Product3, Product4 };
            Invoice invoice = new Invoice(1, "test_name", "q5113823@live.tees.ac.uk", "123 fake street", 1234.43, products); //Note: 'Total price' is not accurate on purpose

            var emailControllerResult = emailController.SendInvoice(invoice).Result;

            Assert.AreEqual(HttpStatusCode.Unauthorized, emailControllerResult);
        }
    }
}
