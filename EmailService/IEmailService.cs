﻿using System.Threading.Tasks;

namespace EmailService
{
    public interface IEmailService
    {
        Task<System.Net.HttpStatusCode> SendEmail(string recipientEmail, string recipientName, string subject, string body, string apiKey);
    }
}
