﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace EmailService
{
    public class EmailService : IEmailService
    {
        public async Task<System.Net.HttpStatusCode> SendEmail(string recipientEmail, 
            string recipientName, 
            string subject,
            string body,
            string apiKey)
        {
            // some validation here!
            if(string.IsNullOrEmpty(apiKey))
            {
                return System.Net.HttpStatusCode.Unauthorized;
            }
            if(string.IsNullOrEmpty(recipientEmail) || string.IsNullOrEmpty(recipientName))
            {
                return System.Net.HttpStatusCode.NotAcceptable;
            }

            //construct message
            var msg = new SendGridMessage();

            msg.SetFrom(new EmailAddress("threeamigos@example.com", "Three Amigos"));
            //msg.SetFrom(new EmailAddress("azure_295b9ad1d76585b9311c637dfdd8a947@azure.com", "Three Amigos"));

            var recipient = new EmailAddress(recipientEmail, recipientName);

            msg.AddTo(recipient);

            msg.SetSubject(subject);

            msg.AddContent(MimeType.Html, body);

            var client = new SendGridClient(apiKey);

            var response = await client.SendEmailAsync(msg);

            return response.StatusCode;
        }
    }
}
