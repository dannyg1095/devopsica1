﻿using PurchasingService.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PurchasingService.Interfaces
{
    public interface IUndercutters
    {
        Task<List<Product>> GetProductAsync(int? catId, int? brandId, double? minPrice, double? maxPrice);
        Task<Product> GetProductAsync(int id);
        Category GetCategory(int id);
        Task<List<Category>> GetCategoriesAsync();
        Brand GetBrand(int id);
        Task<List<Brand>> GetBrandsAsync();
        void PostOrders(List<Order> Orders);
    }
}
