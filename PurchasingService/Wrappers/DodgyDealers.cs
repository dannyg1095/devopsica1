﻿using Newtonsoft.Json;
using PurchasingService.DTOs;
using PurchasingService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PurchasingService.ThirdParty
{
    public class DodgyDealers : IDodgyDealers
    {
        /*
        http://undercutters.azurewebsites.net/
        http://dodgydealers.azurewebsites.net/
        http://bazzasbazaar.azurewebsites.net/Store.svc
        */
        private HttpClient client = new HttpClient();

        public DodgyDealers()
        {
            client.BaseAddress = new Uri("http://dodgydealers.azurewebsites.net/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Product>> GetProductAsync(int? catId, int? brandId, double? minPrice, double? maxPrice)
        {
            List<Product> products = new List<Product>();
            HttpResponseMessage res = await client.GetAsync("api/Product/");

            if (res.IsSuccessStatusCode)
            {
                var prods = await res.Content.ReadAsAsync<List<Product>>();

                products.AddRange(prods
                    .Where(p => catId != null ? catId == p.CategoryId : catId != p.CategoryId)
                    .Where(p => brandId != null ? brandId == p.BrandId : brandId != p.BrandId)
                    .Where(p => minPrice != null ? p.Price >= minPrice : minPrice != p.Price)
                    .Where(p => maxPrice != null ? p.Price <= maxPrice : maxPrice != p.Price)
                    .Select(p => new Product
                    {
                        Id = p.Id,
                        EAN = p.EAN,
                        CategoryId = p.CategoryId,
                        CategoryName = p.CategoryName,
                        BrandId = p.BrandId,
                        BrandName = p.BrandName,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        InStock = p.InStock,
                        ExpectedRestock = p.ExpectedRestock,
                        SupplierName = "DodgyDealers",
                        SupplierId = 2
                    }));
            }

            return products;
        }

        public async Task<Product> GetProductAsync(int Id)
        {
            List<Product> products = new List<Product>();
            HttpResponseMessage res = await client.GetAsync("api/Product/");

            if (res.IsSuccessStatusCode)
            {
                var prods = await res.Content.ReadAsAsync<List<Product>>();

                products.AddRange(prods
                    .Where(p => Id == p.Id)
                    .Select(p => new Product
                    {
                        Id = p.Id,
                        EAN = p.EAN,
                        CategoryId = p.CategoryId,
                        CategoryName = p.CategoryName,
                        BrandId = p.BrandId,
                        BrandName = p.BrandName,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        InStock = p.InStock,
                        ExpectedRestock = p.ExpectedRestock
                    }));
            }

            return products[0];
        }

        public Category GetCategory(int Id)
        {
            return GetCategoriesAsync().Result.FirstOrDefault(c => Id == c.Id);
        }

        public async Task<List<Category>> GetCategoriesAsync()
        {
            List<Category> categories = new List<Category>();
            HttpResponseMessage res = await client.GetAsync("api/Category/");

            if (res.IsSuccessStatusCode)
            {
                var cats = await res.Content.ReadAsAsync<List<Category>>();

                categories.AddRange(cats
                    .Select(c => new Category
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Desc = c.Desc,
                        ProdCount = c.ProdCount
                    }));
            }

            return categories;
        }

        public Brand GetBrand(int Id)
        {
            return GetBrandsAsync().Result.FirstOrDefault(b => Id == b.Id);
        }

        public async Task<List<Brand>> GetBrandsAsync()
        {
            List<Brand> brands = new List<Brand>();
            HttpResponseMessage res = await client.GetAsync("api/Brand/");

            if (res.IsSuccessStatusCode)
            {
                var brans = await res.Content.ReadAsAsync<List<Brand>>();

                brands.AddRange(brans
                    .Select(b => new Brand
                    {
                        Id = b.Id,
                        Name = b.Name,
                        ProdCount = b.ProdCount
                    }));
            }

            return brands;
        }

        public async void PostOrders(List<Order> Orders)
        {
            string s;
            byte[] buffer;
            ByteArrayContent byteContent;
            HttpResponseMessage res;


            foreach (Order o in Orders)
            {
                s = JsonConvert.SerializeObject(o);
                buffer = System.Text.Encoding.UTF8.GetBytes(s);
                byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                res = await client.PostAsync("api/Order", byteContent);
            }
        }
    }
}
