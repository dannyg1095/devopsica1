﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Configuration;
using PurchasingService.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PurchasingService
{
    public class PurchasingServiceDbContext : DbContext
    {
        public DbSet<Resell> Resells { get; set; }
        public DbSet<PurchaseRequest> PurchaseRequests { get; set; }
        public DbSet<Order> PurchaseRequestOrders { get; set; }

        public PurchasingServiceDbContext()
        {

        }

        public PurchasingServiceDbContext(DbContextOptions<PurchasingServiceDbContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot config = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseSqlServer(config.GetConnectionString("PurchasingServiceConnectionString"));
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Resell>().ToTable("ResellHistory");

            modelBuilder.Entity<PurchaseRequest>(pr =>
            {
                pr.Property(p => p.EmployeeId).IsRequired();
                pr.Property(p => p.SupplierId).IsRequired();
                pr.Property(p => p.SupplierName).IsRequired();
                pr.Property(p => p.PriceTotal).IsRequired();
                pr.Property(p => p.Active).HasDefaultValue(true);
            });
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}