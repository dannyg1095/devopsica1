﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PurchasingService.ThirdParty;
using PurchasingService.Interfaces;
using Microsoft.EntityFrameworkCore;
using PurchasingService.Repositories;
using Swashbuckle.AspNetCore.Swagger;
using Newtonsoft.Json.Serialization;
using System.IdentityModel.Tokens.Jwt;

namespace PurchasingService
{
    public class Startup //Demonstration commit
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "API Docs", Version = "v1" });
            });

            IConfigurationRoot config = new ConfigurationBuilder()
               .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
               .AddJsonFile("appsettings.json")
               .Build();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services.AddAuthentication("Bearer")
                    .AddJwtBearer("Bearer", options =>
                    {
                        options.Audience = config.GetSection("OAuth2").GetSection("Audience").Value;
                        options.Authority = config.GetSection("OAuth2").GetSection("Authority").Value;
                    });

            services.AddMvc().AddJsonOptions(opt => opt.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddTransient<IUndercutters, Undercutters>();
            services.AddTransient<IDodgyDealers, DodgyDealers>();
            services.AddTransient<IBazzasBazaar, BazzasBazaar>();
            services.AddDbContext<PurchasingServiceDbContext>();
            services.AddTransient<IPurchaseRequestRepository, PurchaseRequestRepository>();
            services.AddTransient<IResellRepository, ResellRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                c.RoutePrefix = String.Empty;
            });

            app.UseAuthentication();

            app.UseHttpsRedirection();
            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{actions=Index}/{id?}");
            });
        }
    }
}
