﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PurchasingService.DTOs
{
    public class Order
    {
        public Order() { }

        public Order(int Id, string AccountName, string CardNumber, int ProductId, short Quantity, DateTime When, string ProductName, string ProductEan, double TotalPrice, int PurchaseRequestId)
        {
            this.Id = Id;
            this.ProductId = ProductId;
            this.Quantity = Quantity;
            this.When = When;
            this.ProductName = ProductName;
            this.ProductEan = ProductEan;
            this.TotalPrice = TotalPrice;
            this.PurchaseRequestId = PurchaseRequestId;
            Active = true;
        }

        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public short Quantity { get; set; }
        public DateTime When { get; set; }
        public string ProductName { get; set; }
        public string ProductEan { get; set; }
        public double TotalPrice { get; set; }
        public int? PurchaseRequestId { get; set; }
        public bool? Active { get; set; } = true;
    }
}
