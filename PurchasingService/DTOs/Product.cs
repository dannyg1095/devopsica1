﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PurchasingService.DTOs
{
    /*
        {
            "Id": 1,
            "Ean": "5 102310 300410",
            "CategoryId": 1,
            "CategoryName": "Screen Protectors",
            "BrandId": 1,
            "BrandName": "iStuff-R-Us",
            "Name": "Rippled Screen Protector",
            "Description": "For his or her sensory pleasure. Fits few known smartphones.",
            "Price": 6.54,
            "InStock": true,
            "ExpectedRestock": null
        }
    */
    public class Product
    {
        public Product()
        {

        }

        public Product(int Id, string EAN, int SupplierId, string SupplierName, int CatId, string CatName, int BrandId, string BrandName, string ProductName, string ShortDesc, int Amount, double Price, bool InStock, DateTime? ExpectedRestock, short? Quantity = null)
        {
            this.Id = Id;
            this.EAN = EAN;
            this.SupplierId = SupplierId;
            this.SupplierName = SupplierName;
            this.CategoryId = CatId;
            this.CategoryName = CatName;
            this.BrandId = BrandId;
            this.BrandName = BrandName;
            this.Name = ProductName;
            this.Description = ShortDesc;
            this.Price = Price;
            this.InStock = InStock;
        }

        public int Id { get; set; }
        public string EAN { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public bool InStock { get; set; }
        public DateTime? ExpectedRestock { get; set; }
    }
}
