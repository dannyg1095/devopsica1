﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PurchasingService.DTOs
{
    public class Resell
    {
        public Resell()
        { }

        public Resell(int EmployeeId, string EAN, double ResellPrice)
        {
            this.EmployeeId = EmployeeId;
            this.EAN = EAN;
            this.ResellPrice = ResellPrice;
            this.Date = DateTime.Now;
        }

        public Resell(int EmployeeId, string EAN, double ResellPrice, DateTime Date)
        {
            this.EmployeeId = EmployeeId;
            this.EAN = EAN;
            this.ResellPrice = ResellPrice;
            this.Date = Date;
        }

        [Key]
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public string EAN { get; set; }
        public double ResellPrice { get; set; }
        public DateTime Date { get; set;  }
    }
}
