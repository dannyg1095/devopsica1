﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PurchasingService.DTOs
{
    public class SendOrder : Order
    {
        public SendOrder() { }

        public SendOrder(int Id, string AccountName, string CardNumber, int ProductId, short Quantity, DateTime When, string ProductName, string ProductEan, double TotalPrice, int PurchaseRequestId)
        {
            this.Id = Id;
            this.AccountName = AccountName;
            this.CardNumber = CardNumber;
            this.ProductId = ProductId;
            this.Quantity = Quantity;
            this.When = When;
            this.ProductName = ProductName;
            this.ProductEan = ProductEan;
            this.TotalPrice = TotalPrice;
            this.PurchaseRequestId = PurchaseRequestId;
            Active = true;
        }

        public string AccountName;
        public string CardNumber;
    }
}
