﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PurchasingService.DTOs
{
    public class Brand
    {
        public Brand() { }
        public Brand(int Id, string Name, int ProdCount)
        {
            this.Id = Id;
            this.Name = Name;
            this.ProdCount = ProdCount;
        }

        public int Id;
        public string Name;
        public int ProdCount;
    }
}
