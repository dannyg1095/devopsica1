﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PurchasingService.DTOs
{
    public class PurchaseRequest
    {
        public PurchaseRequest() { }

        public PurchaseRequest(int EmployeeId, int SupplierId, string SupplierName, double PriceTotal, Order[] Orders)
        {
            this.EmployeeId = EmployeeId;
            this.SupplierId = SupplierId;
            this.SupplierName = SupplierName;
            this.PriceTotal = PriceTotal;
            this.Orders = Orders;
            Date = DateTime.Now;
            Active = true;
        }

        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int EmployeeId { get; set; }
        public int SupplierId { get; set; }
        public string SupplierName { get; set; }
        public double PriceTotal { get; set; }
        public bool? Approved { get; set; }
        public int? ApprovedBy { get; set; }
        public IEnumerable<Order> Orders { get; set; } //One to many relationship
        public bool Active { get; set; } = true;
    }
}
