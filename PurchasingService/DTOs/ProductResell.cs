﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace PurchasingService.DTOs
{
    public class ProductResell
    {
        public ProductResell(string EAN, double Price)
        {
            this.EAN = EAN;
            this.Price = Price;
        }

        public string EAN;
        public double Price;
    }
}
