﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PurchasingService.DTOs
{
    public class ApprovalPostApprove
    {
        public int ManagerId;
        public List<int> PurReqIds;
        public string CardName;
        public string CardNo;
    }
}
