﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PurchasingService.DTOs
{
    public class Category
    {
        public Category() { }

        public Category(int Id, string Name, string Desc, int ProdCount)
        {
            this.Id = Id;
            this.Name = Name;
            this.Desc = Desc;
            this.ProdCount = ProdCount;
        }

        public int Id;
        public string Name;
        public string Desc;
        public int ProdCount;
    }
}
