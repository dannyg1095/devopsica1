﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PurchasingService.DTOs
{
    public class ApprovalPostDeny
    {
        public int ManagerId;
        public List<int> PurReqIds;
    }
}
