﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PurchasingService.DTOs;

namespace PurchasingService.Repositories
{
    public class ResellRepository : IResellRepository
    {
        private readonly PurchasingServiceDbContext _context;

        public ResellRepository(PurchasingServiceDbContext context)
        {
            _context = context;
        }

        //Not in the requirements but added for the sake of the model
        public void Delete(int Id)
        {
            throw new NotImplementedException();
        }

        public List<Resell> GetByEan(string Ean, DateTime From, DateTime To)
        {
            List<Resell> Resells = new List<Resell>();

            Resells.AddRange(_context.Resells
                .Where(r => r.EAN.Equals(Ean))
                .Where(r => DateTime.Compare(r.Date, From) >= 0 && DateTime.Compare(r.Date, To) <= 0));

            return Resells;
        }

        public List<Resell> GetById(int EmployeeId, DateTime From, DateTime To)
        {
            List<Resell> Resells = new List<Resell>();

            Resells.AddRange(_context.Resells
                .Where(r => r.EmployeeId == EmployeeId)
                .Where(r => DateTime.Compare(r.Date, From) >= 0 && DateTime.Compare(r.Date, To) <= 0));

            return Resells;
        }

        public List<Resell> GetByDate(DateTime From, DateTime To)
        {
            List<Resell> Resells = new List<Resell>();

            Resells.AddRange(_context.Resells
                .Where(r => DateTime.Compare(r.Date, From) >= 0 && DateTime.Compare(r.Date, To) <= 0));

            return Resells;
        }

        public void Insert(int EmployeeId, List<ProductResell> Products)
        {
            foreach (ProductResell p in Products)
            {
                _context.Add(new Resell(EmployeeId, p.EAN, (p.Price + (p.Price / 10) * 2))); //pass EmployeeId, EAN, and (price + (20% of price))
            }
            _context.SaveChanges();
        }

        public List<Resell> Search(Expression<Func<Resell, bool>> predicate)
        {
            throw new NotImplementedException();
        }
    }
}
