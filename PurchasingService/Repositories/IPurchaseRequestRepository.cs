﻿using PurchasingService.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PurchasingService.Repositories
{
    public interface IPurchaseRequestRepository
    {
        void Insert(PurchaseRequest pr);
        void InsertMany(PurchaseRequest[] prs);
        void Delete(int Id);
        List<PurchaseRequest> Search(Expression<Func<PurchaseRequest, bool>> predicate);
        List<PurchaseRequest> GetAll(DateTime From, DateTime To);
        PurchaseRequest GetById(int Id);
        List<Order> Approve(int ManagerId, List<int> PurReqIds, string AccountName, string CardNumber);
        void Deny(int ManagerId, List<int> PurReqIds);
    }
}
