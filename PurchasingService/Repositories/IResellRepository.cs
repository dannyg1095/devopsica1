﻿using PurchasingService.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PurchasingService.Repositories
{
    public interface IResellRepository
    {
        void Insert(int EmployeeId, List<ProductResell> Products);
        void Delete(int Id);
        List<Resell> Search(Expression<Func<Resell, bool>> predicate);
        List<Resell> GetByEan(string Ean, DateTime From, DateTime To);
        List<Resell> GetByDate(DateTime From, DateTime To);
        List<Resell> GetById(int EmployeeId, DateTime From, DateTime To);
    }
}
