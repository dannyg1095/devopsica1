﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PurchasingService.DTOs;

namespace PurchasingService.Repositories
{
    public class PurchaseRequestRepository : IPurchaseRequestRepository
    {
        private readonly PurchasingServiceDbContext _context;

        public PurchaseRequestRepository(PurchasingServiceDbContext context)
        {
            _context = context;
        }

        //Not in the requirements but added for the sake of the model
        public void Delete(int Id)
        {
            PurchaseRequest PurchaseRequest = new PurchaseRequest();
            PurchaseRequest = _context.PurchaseRequests.Where(pr => pr.Id == Id).FirstOrDefault();
            PurchaseRequest.Active = false;

            List<Order> Orders = new List<Order>();
            Orders.AddRange(_context.PurchaseRequestOrders.Where(o => o.PurchaseRequestId == Id));

            foreach (Order p in Orders)
            {
                p.Active = false;
            }

            _context.Update(PurchaseRequest);
            _context.UpdateRange(Orders);

            _context.SaveChanges();
        }

        public List<PurchaseRequest> GetAll(DateTime From, DateTime To)
        {
            List<PurchaseRequest> PurchaseRequests = new List<PurchaseRequest>();

            PurchaseRequests.AddRange(_context.PurchaseRequests
                .Where(pr => pr.Active)
                .Where(pr => DateTime.Compare(pr.Date, From) >= 0 && DateTime.Compare(pr.Date, To) <= 0)
                .Select(pr => new PurchaseRequest
                {
                    Id = pr.Id,
                    Date = pr.Date,
                    EmployeeId = pr.EmployeeId,
                    SupplierId = pr.SupplierId,
                    SupplierName = pr.SupplierName,
                    PriceTotal = pr.PriceTotal,
                    Approved = pr.Approved,
                    ApprovedBy = pr.ApprovedBy,
                    Orders = new List<Order>()
                }));

            foreach (PurchaseRequest pr in PurchaseRequests)
            {
                List<Order> orders = new List<Order>();

                orders.AddRange(_context.PurchaseRequestOrders
                    .Where(o => pr.Id.Equals(o.PurchaseRequestId))
                    .Select(o => new Order
                    {
                        Id = o.Id,
                        ProductId = o.ProductId,
                        Quantity = o.Quantity,
                        When = o.When,
                        ProductName = o.ProductName,
                        ProductEan = o.ProductEan,
                        TotalPrice = o.TotalPrice,
                        PurchaseRequestId = o.PurchaseRequestId,
                        Active = true
                    }));

                pr.Orders = orders;
            }

            return PurchaseRequests;
        }

        public PurchaseRequest GetById(int Id)
        {
            return _context.PurchaseRequests.Where(pr => pr.Id == Id).FirstOrDefault();
        }

        public void Insert(PurchaseRequest pr)
        {
            pr.Date = DateTime.Now;

            _context.Add(pr);

            _context.SaveChanges();
        }

        public void InsertMany(PurchaseRequest[] prs)
        {
            DateTime dt = DateTime.Now;
            foreach (PurchaseRequest pr in prs)
            {
                pr.Date = dt;
            }

            _context.AddRange(prs);

            _context.SaveChanges();
        }

        public List<PurchaseRequest> Search(Expression<Func<PurchaseRequest, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public List<Order> Approve(int ManagerId, List<int> PurReqIds, string AccountName, string CardNumber)
        {
            List<PurchaseRequest> prs = new List<PurchaseRequest>();
            List<Order> Orders = new List<Order>();
            PurchaseRequest pr;

            foreach (int Id in PurReqIds)
            {
                pr = _context.PurchaseRequests.Where(p => p.Id == Id).FirstOrDefault();
                pr.Approved = true;
                pr.ApprovedBy = ManagerId;

                prs.Add(pr);
                Orders.AddRange(_context.PurchaseRequestOrders
                                    .Where(o => o.PurchaseRequestId == Id)
                                    .Select(o => new SendOrder
                                    {
                                        AccountName = AccountName,
                                        CardNumber = CardNumber,
                                        Id = o.Id,
                                        ProductId = o.ProductId,
                                        Quantity = o.Quantity,
                                        When = o.When,
                                        ProductName = o.ProductName,
                                        ProductEan = o.ProductEan,
                                        TotalPrice = o.TotalPrice
                                    }));
            }

            _context.UpdateRange(prs);

            _context.SaveChanges();

            return Orders;
        }

        public void Deny(int ManagerId, List<int> PurReqIds)
        {
            List<PurchaseRequest> prs = new List<PurchaseRequest>();
            PurchaseRequest pr;

            foreach (int Id in PurReqIds)
            {
                pr = _context.PurchaseRequests.Where(p => p.Id == Id).FirstOrDefault();
                pr.Approved = false;
                pr.ApprovedBy = ManagerId;

                prs.Add(pr);
            }

            _context.UpdateRange(prs);

            _context.SaveChanges();
        }
    }
}
