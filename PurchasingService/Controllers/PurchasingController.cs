﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PurchasingService.DTOs;
using PurchasingService.Interfaces;
using PurchasingService.Repositories;
using PurchasingService.ThirdParty;

namespace PurchasingService.Controllers
{
    [Route("api/purchasing")]
    [ApiController]
    [Authorize]
    public class PurchasingController : ControllerBase
    {
        protected IUndercutters _undercutters;
        protected IDodgyDealers _dodgydealers;
        protected IBazzasBazaar _bazzasbazaar;
        private readonly IPurchaseRequestRepository PurReqRepository;

        public PurchasingController(IPurchaseRequestRepository prr, IUndercutters uc, IDodgyDealers dd, IBazzasBazaar bb)
        {
            _undercutters = uc;
            _dodgydealers = dd;
            _bazzasbazaar = bb;
            PurReqRepository = prr;
        }

        // GET api/purchasing/VARS
        [HttpGet]
        [ActionName("GetProducts")]
        public List<Product> GetProducts(int? CatId = null, int? BrandId = null, double? MinPrice = null, double? MaxPrice = null)
        {
            List<Product> products = new List<Product>();

            products.AddRange(_undercutters.GetProductAsync(CatId, BrandId, MinPrice, MaxPrice).Result);
            products.AddRange(_dodgydealers.GetProductAsync(CatId, BrandId, MinPrice, MaxPrice).Result);
            products.AddRange(_bazzasbazaar.GetProductAsync(CatId, BrandId, MinPrice, MaxPrice).Result);

            return products;
        }

        [HttpGet("GetCategories")]
        public List<Category> GetCategories()
        {
            return _undercutters.GetCategoriesAsync().Result;
        }

        [HttpGet("GetCategory/{CatId}")]
        public Category GetCategory(int CatId)
        {
            return _undercutters.GetCategory(CatId);
        }

        [HttpGet("GetBrands")]
        public List<Brand> GetBrands()
        {
            return _undercutters.GetBrandsAsync().Result;
        }

        [HttpGet("GetBrand/{BrandId}")]
        public Brand GetBrands(int BrandId)
        {
            return _undercutters.GetBrand(BrandId);
        }

        // POST api/purchasing/VARS
        [HttpPost("PurchaseRequest")]
        public void Post(PurchaseRequest pr)
        {
            PurReqRepository.Insert(pr);
        }
        
        [HttpPost("PurchaseRequests")]
        public void Post(PurchaseRequest[] prs)
        {
            PurReqRepository.InsertMany(prs);
        }
    }
}
