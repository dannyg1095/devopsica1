﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PurchasingService.DTOs;
using PurchasingService.Repositories;

namespace PurchasingService.Controllers
{
    [Route("api/resell")]
    [ApiController]
    [Authorize]
    public class ResellController : ControllerBase
    {
        //add 20% to resell price for new products

        /*
         * POST(create & edit) setResell{employeeId, prodsArray}
         * GET getResellHistory{prodIdArray?}
         * GET getResellHistoryByPandD{prodIdArray, from?, to}
         * GET getResellHistoryByDate{from?, to}
        */

        private readonly IResellRepository ResellRepository;

        public ResellController(IResellRepository rr)
        {
            ResellRepository = rr;
        }

        // POST api/resell
        [HttpPost]
        public void SetResell(SetResellDTO srd)//(int EmployeeId, ProductResell[] Products)
        {
            ResellRepository.Insert(srd.EmployeeId, srd.Products);
        }

        // GET api/resell/GetResellHistoryByDate
        [HttpGet]
        public List<Resell> GetResell(DateTime? From = null, DateTime? To = null)
        {
            //null checks
            DateTime F = (From is null) ? DateTime.Now.AddDays(-14) : (DateTime)From;
            DateTime T = (To is null) ? DateTime.Now : (DateTime)To;

            return ResellRepository.GetByDate(F, T);
        }

        // GET api/resell
        [HttpGet("GetResellByEan")]
        public List<Resell> GetResellByEan(string Ean, DateTime? From = null, DateTime? To = null)
        {
            //null checks
            DateTime F = (From is null) ? DateTime.Now.AddDays(-14) : (DateTime) From;
            DateTime T = (To is null) ? DateTime.Now : (DateTime) To;

            return ResellRepository.GetByEan(Ean, F, T);
        }

        // GET api/resell/GetResellById
        [HttpGet("GetResellById")]
        public List<Resell> GetResellById(int EmployeeId, DateTime? From = null, DateTime? To = null)
        {
            //null checks
            DateTime F = (From is null) ? DateTime.Now.AddDays(-14) : (DateTime)From;
            DateTime T = (To is null) ? DateTime.Now : (DateTime)To;

            return ResellRepository.GetById(EmployeeId, F, T);
        }

        
    }
}