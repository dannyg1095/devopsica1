﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PurchasingService.DTOs;
using PurchasingService.Interfaces;
using PurchasingService.Repositories;
using PurchasingService.ThirdParty;

namespace PurchasingService.Controllers
{
    [Route("api/approval")]
    [ApiController]
    [Authorize]
    public class ApprovalController : ControllerBase
    {
        protected IUndercutters _undercutters;
        protected IDodgyDealers _dodgydealers;
        protected IBazzasBazaar _bazzasbazaar;
        protected IPurchaseRequestRepository PurReqRepository;

        public ApprovalController(IPurchaseRequestRepository prr, IUndercutters uc, IDodgyDealers dd, IBazzasBazaar bb)
        {
            PurReqRepository = prr;
            _undercutters = uc;
            _dodgydealers = dd;
        }

        // POST api/approval
        //Doesn't like (int ManagerId, List<int> PurReqIds, string CardName, string CardNo)
        [HttpPost("Approve")]
        public void Post(ApprovalPostApprove apa)
        {
            List<Order> AllOrders = PurReqRepository.Approve(apa.ManagerId, apa.PurReqIds, apa.CardName, apa.CardNo);
            List<Order> Orders = new List<Order>();
            PurchaseRequest pr;
            foreach (int Id in apa.PurReqIds)
            {
                pr = PurReqRepository.GetById(Id);
                Orders = AllOrders.Where(o => o.PurchaseRequestId == pr.Id).ToList();

                switch (pr.SupplierId)
                {
                    case 1:
                        _undercutters.PostOrders(Orders);
                        break;
                    case 2:
                        _dodgydealers.PostOrders(Orders);
                        break;
                    case 3:
                        _bazzasbazaar.PostOrders(Orders);
                        break;
                }
            }
        }

        [HttpPost("Deny")]
        public void Post(ApprovalPostDeny apd)
        {
            PurReqRepository.Deny(apd.ManagerId, apd.PurReqIds);
        }

        // GET api/approval/GetPurReqsByDate/
        [HttpGet]
        public List<PurchaseRequest> GetPurReqByDate(DateTime? From, DateTime? To)
        {
            //null checks
            DateTime F = (From is null) ? DateTime.Now.AddDays(-14) : (DateTime)From;
            DateTime T = (To is null) ? DateTime.Now : (DateTime)To;

            return PurReqRepository.GetAll(F, T);
        }
    }
}
